package main.java.factorial_calculator;

public class Request implements Runnable {
    private int factorialNumber;

    public Request(int factorialNumber) {
        this.factorialNumber = factorialNumber;
    }

    @Override
    public void run() {
        try {
            if (factorialNumber < 0) {
                System.out.println("Number >= 0!");
            } else {
                Thread.sleep(100*factorialNumber);
                System.out.println(factorialNumber + "! = " + factorial(factorialNumber));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private int factorial(int number){
        int factorial = 1;
        if(number > 1){
            factorial = number*factorial(number - 1);
        }
        return factorial;
    }



}
