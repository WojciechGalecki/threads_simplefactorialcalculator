package main.java.factorial_calculator;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AsynchronousFactorialCalculator {

    private Executor factorials = Executors.newFixedThreadPool(5);

    public void calculateFactorial(int factorialNumber){
        Request newRequest = new Request(factorialNumber);
        factorials.execute(newRequest);
    }
}
