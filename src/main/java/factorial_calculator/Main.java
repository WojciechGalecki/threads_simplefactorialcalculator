package main.java.factorial_calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        AsynchronousFactorialCalculator calculator = new AsynchronousFactorialCalculator();
        System.out.println("Welcome to simple factorial calculator" + "\n Give me number: ");
        while (true) {
            try {
                String command = sc.next().trim().toLowerCase();
                if (command.equals("quit")) {
                    return;
                } else {
                    calculator.calculateFactorial(Integer.parseInt(command));
                }
            } catch (NumberFormatException nFe) {
                nFe.printStackTrace();
            }
        }
    }
}
